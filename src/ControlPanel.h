/*
 * Copyright (c) 2024 https://www.trainbrains.eu (Greg Barski).
 * This product is licensed under an GNU GPL, see the LICENSE file in the top-level directory.
 */

#ifndef TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_CONTROL_PANEL_H
#define TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_CONTROL_PANEL_H

#include "PointPosition.h"
#include "ButtonFunction.h"
#include "TrainbrainsModulesConfig.h"

class ControlPanel
{

public:
#if ALLOW_USING_LAMBDAS == USING_LAMBDAS_ENABLED
    typedef std::function<void(ButtonFunction)> OnButtonPressed;
#else
    typedef void (*OnButtonPressed)(ButtonFunction);
#endif

    volatile uint8_t i2cAddress;
    volatile uint8_t channel;

    ControlPanel(uint8_t anAddress)
    {
        i2cAddress = anAddress;
        channel = 1;
    }

    ControlPanel(uint8_t anAddress, uint8_t aChannel)
    {
        i2cAddress = anAddress;
        channel = aChannel;
    }

    ControlPanel(const ControlPanel &other);

    ~ControlPanel()
    {
        onButtonPressedHandler = NULL;
    }

    bool notifyButtonPressed(ButtonFunction actualButtonFunction)
    {
        if (actualButtonFunction == state)
        {
            return false;
        }

        state = actualButtonFunction;

        if (state != ButtonFunction::BUTTON_FUNCTION_NONE && onButtonPressedHandler != nullptr)
        {
            onButtonPressedHandler(state);
            return true;
        }

        return false;
    }

    void notifyButtonReleased()
    {
        state = ButtonFunction::BUTTON_FUNCTION_NONE;
    }

    void whenButtonPressed(OnButtonPressed aHandler)
    {
        onButtonPressedHandler = aHandler;
    }

private:
    OnButtonPressed onButtonPressedHandler = NULL;
    volatile ButtonFunction state = ButtonFunction::BUTTON_FUNCTION_NONE;
};

typedef ControlPanel *ControlPanelRef;

#endif // TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_CONTROL_PANEL_H
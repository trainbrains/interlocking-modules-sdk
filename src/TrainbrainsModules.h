/*
 * Copyright (c) 2024 https://www.trainbrains.eu (Greg Barski).
 * This product is licensed under an GNU GPL, see the LICENSE file in the top-level directory.
 */

#ifndef TRAINBRAINS_INTERLOCKING_MODULES_INFRASTRUCTURE
#define TRAINBRAINS_INTERLOCKING_MODULES_INFRASTRUCTURE

#include "TrainbrainsModulesConfig.h"

// Don't forget the Wire library
#ifndef ARDUINO_AVR_GEMMA
// TinyWireM is now part of
//   Adafruit version of Wire Library, so this
// will work with Adafruit ATtiny85's
// But Arduino Gemma doesn't use that library
// We do NOT want to include Wire if it's an arduino Gemma
#include <Wire.h>
#else
#include <TinyWireM.h>
#define Wire TinyWireM
#endif

#include "DeviceInfo.h"
#include "MessageFrames.h"
#include "LightSignal.h"
#include "Point.h"
#include "TrackUnoccupancyDetector.h"
#include "ControlPanel.h"
#include "ControlDesktop.h"
#include "Mimics.h"
#include "ButtonFunction.h"
#include "Repository.h"
#include "TracksideDeviceType.h"

class TrainbrainsModules
{

private:
#define MAX_NUMBER_OF_TRACKSIDE_OBJECTS 32
#define MAX_NUMBER_OF_CONTROL_DESKTOPS 8
#define TRACKSIDE_OBJECTS_STATE_CHECK_INTERVAL 1000UL
#define CONTROL_PANEL_BUTONS_CHECK_INTERVAL 200UL

    TwoWire *i2c;
    MessageFrame lastResponse = EMPTY_MESSAGE_FRAME;
    static const unsigned long DEFAULT_AWAIT_TIMEOUT_IN_MILLIS = 100UL;

    volatile unsigned long nextTracksideObjectsStateCheck = 0UL;
    volatile unsigned long nextControlPanelButtonsCheck = 0UL;

    volatile unsigned long nextControlDesktopUpdate = 0UL;

    Repository<TracksideObjectRef> *tracksideObjects = NULL;
    Repository<ControlDesktopRef> *controlDesktops = NULL;

    bool isBusy = false;

    void setup(int maxNumberOfTracksideObjects = MAX_NUMBER_OF_TRACKSIDE_OBJECTS,
               int maxNumberOfControlDesktopsObjects = MAX_NUMBER_OF_CONTROL_DESKTOPS);

    bool isConnected(byte anAddress)
    {
        i2c->beginTransmission(anAddress);
        return i2c->endTransmission() == 0;
    }

    uint8_t sendFrame(MessageFrame frame)
    {
        if (!isConnected(frame[0]))
            return 1;
        i2c->endTransmission();
        i2c->beginTransmission(frame[0]);
        i2c->write(frame, MESSAGE_FRAME_LENGTH);
        uint8_t result = i2c->endTransmission();
        delete frame;
        frame = NULL;
        return result;
    }

    MessageFrame awaitResult(uint8_t address)
    {
        for (uint8_t i = 0; i < MESSAGE_FRAME_LENGTH; i++)
        {
            lastResponse[i] = 0;
        }
        i2c->requestFrom(address, (uint8_t)MESSAGE_FRAME_LENGTH);
        unsigned long timeoutInMillis = millis() + DEFAULT_AWAIT_TIMEOUT_IN_MILLIS;
        while (millis() < timeoutInMillis)
        {
            if (i2c->available() >= MESSAGE_FRAME_LENGTH)
            {
                for (int i = 0; i < MESSAGE_FRAME_LENGTH; i++)
                {
                    lastResponse[i] = i2c->read();
                }
                break;
            }
        }

        return lastResponse;
    }

    MessageFrame awaitResultOrAssumeAcknowledge(uint8_t address)
    {

#if AWAIT_COMMAND_RESULT == AWAIT_COMMAND_RESULT_ENABLED
        return awaitResult(address);
#elif AKNOWLEDGE_I2C_COMMANDS == AKNOWLEDGE_I2C_COMMANDS_ENABLED
        return acknowledgeFrame(address);
#else
        return EMPTY_MESSAGE_FRAME;
#endif
    }

public:
    TrainbrainsModules(TwoWire *anI2c)
    {
        i2c = anI2c;
        setup(MAX_NUMBER_OF_TRACKSIDE_OBJECTS, MAX_NUMBER_OF_CONTROL_DESKTOPS);
    }

    TrainbrainsModules(
        TwoWire *anI2c = &Wire,
        int maxNumberOfTracksideObjects = MAX_NUMBER_OF_TRACKSIDE_OBJECTS,
        int maxNumberOfControlDesktopsObjects = MAX_NUMBER_OF_CONTROL_DESKTOPS)
    {
        i2c = anI2c;
        setup(MAX_NUMBER_OF_TRACKSIDE_OBJECTS, MAX_NUMBER_OF_CONTROL_DESKTOPS);
    }

    ~TrainbrainsModules()
    {
        delete tracksideObjects;
        delete controlDesktops;
        i2c->end();
    }

    void init();

    uint8_t addressDevice(TracksideObjectRef tracksideDevice);

    MessageFrame switchPointPosition(PointRef point);

    MessageFrame switchPointPosition(PointRef point, PointPosition expectedPosition);

    MessageFrame adjustPointSwitchingTime(PointRef point, uint8_t expectedSwitchingTimeInSeconds);

    MessageFrame checkDeviceState(uint8_t address, uint8_t channel);

    PointPosition checkDeviceState(PointRef point);

    SignalType checkDeviceState(LightSignalRef signal);

    TrackUnoccupancy checkDeviceState(TrackUnoccupancyDetectorRef trackOccupancyDetector);

    TracksideDeviceType checkDeviceType(uint8_t i2cAddress);

    uint8_t checkDeviceChannelsCount(uint8_t i2cAddress);

    uint8_t getTracksideObjectState(String objectId);

    MessageFrame setSignalAspectIndex(LightSignalRef lightSignal, uint8_t signalIndex);

    MessageFrame setSignalAspect(LightSignalRef lightSignal, SignalAspect signalAspect);

    MessageFrame displayMimic(ControlPanelRef controlPanel, Mimics mimic);

    MessageFrame displayMimic(ControlPanelRef controlPanel, PointPosition pointPosition);

    MessageFrame displayMimic(ControlPanelRef controlPanel, SignalType signalType);

    MessageFrame displayMimic(ControlPanelRef controlPanel, TrackUnoccupancy trackUnoccupancy);

    MessageFrame displayMimic(uint8_t i2cAddress, uint8_t channel, PointPosition pointPosition);

    MessageFrame displayMimic(uint8_t i2cAddress, uint8_t channel, SignalType signalType);

    MessageFrame displayMimic(uint8_t i2cAddress, uint8_t channel, TrackUnoccupancy trackUnoccupancy);

    MessageFrame displayMimic(uint8_t i2cAddress, uint8_t channel, Mimics mimic);

    bool use(TracksideObjectRef tracksideObject);

    bool use(ControlDesktopRef controlDesktop);

    bool checkUsedObjectsState(unsigned long interval = TRACKSIDE_OBJECTS_STATE_CHECK_INTERVAL);

    bool checkUsedControlDesktops(unsigned long interval = CONTROL_PANEL_BUTONS_CHECK_INTERVAL);

    ButtonFunction checkPanelButtons(ControlPanelRef panel);

    ButtonFunction checkDesktopPanelButtons(ControlDesktopRef desktop);

    bool commandIsAccepted(MessageFrame message);

    bool updateTracksideObjectState(String objectId, uint8_t newObjectState);

    bool updateTracksideObjectState(TracksideObjectRef tracksideObject, uint8_t newObjectState);

    void removeAll();
};

typedef TrainbrainsModules *TrainbrainsModulesRef;

#endif // TRAINBRAINS_INTERLOCKING_MODULES_INFRASTRUCTURE
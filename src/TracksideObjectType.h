#ifndef TRACKSIDE_OBJECT_TYPE_H
#define TRACKSIDE_OBJECT_TYPE_H

enum TracksideObjectType
{
    UNKNOWN_TRACKSIDE_OBJECT_TYPE = 0,
    TRACKSIDE_OBJECT_TYPE_SIGNAL = 1,
    TRACKSIDE_OBJECT_TYPE_POINT = 2,
    TRACKSIDE_OBJECT_TYPE_TRACK_POWER_DRIVER = 3,
    TRACKSIDE_OBJECT_TYPE_TRACK_UNOCCUPANCY_DETECTOR = 4,
    TRACKSIDE_OBJECT_TYPE_AXEL_COUNTER = 5,
    TRACKSIDE_OBJECT_TYPE_DISTANT_SIGNAL = 6,

};

static String nameOf(TracksideObjectType objectType)
{
    switch (objectType)
    {
    case TracksideObjectType::TRACKSIDE_OBJECT_TYPE_SIGNAL:
        return "Signal";
    case TracksideObjectType::TRACKSIDE_OBJECT_TYPE_POINT:
        return "Point";
    case TracksideObjectType::UNKNOWN_TRACKSIDE_OBJECT_TYPE:
        return "Unknown";
    case TracksideObjectType::TRACKSIDE_OBJECT_TYPE_AXEL_COUNTER:
        return "Axels counter";
    case TracksideObjectType::TRACKSIDE_OBJECT_TYPE_TRACK_POWER_DRIVER:
        return "Track power driver";  
    case TracksideObjectType::TRACKSIDE_OBJECT_TYPE_TRACK_UNOCCUPANCY_DETECTOR:
        return "Inoccupancy detector";
    case TracksideObjectType::TRACKSIDE_OBJECT_TYPE_DISTANT_SIGNAL:
        return "Distant signal";
    default:
        return "?";
    }
}

#endif // TRACKSIDE_OBJECT_TYPE_H
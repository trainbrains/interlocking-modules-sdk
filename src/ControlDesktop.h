/*
 * Copyright (c) 2024 https://www.trainbrains.eu (Greg Barski).
 * This product is licensed under an GNU GPL, see the LICENSE file in the top-level directory.
 */

#ifndef TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_CONTROL_DESKTOP_H
#define TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_CONTROL_DESKTOP_H

#include "TrainbrainsModulesConfig.h"

#define DEFAULT_TRACKSIDE_DEVICE_CHANNEL 1

#if ALLOW_USING_LAMBDAS == USING_LAMBDAS_ENABLED
    typedef std::function<void(uint8_t controlDesktopI2cAddress, uint8_t channel, ButtonFunction buttonFunction)> OnControlDesktopButtonPressed;
#else    
    typedef void (*OnControlDesktopButtonPressed)(uint8_t controlDesktopI2cAddress, uint8_t channel, ButtonFunction buttonFunction);
#endif 

class ControlDesktop
{

public:
    volatile uint8_t i2cAddress;
    volatile uint8_t numberOfChannels;

    ControlDesktop(uint8_t anI2cAddress, uint8_t aNumberOfChannels)
    {
        i2cAddress = anI2cAddress;
        numberOfChannels = aNumberOfChannels;
    }

    ControlDesktop(const ControlDesktop &other);    

    virtual bool notifyButtonPressed(uint8_t channel, ButtonFunction actualButtonFunction);
    virtual bool notifyButtonsReleased();
    virtual void handleButtonPressed(OnControlDesktopButtonPressed handler);
};

typedef ControlDesktop *ControlDesktopRef;

#endif //TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_CONTROL_DESKTOP_H
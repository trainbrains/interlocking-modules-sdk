/*
 * Copyright (c) 2024 https://www.trainbrains.eu (Greg Barski).
 * This product is licensed under an GNU GPL, see the LICENSE file in the top-level directory.
 */

#ifndef TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_TRACK_UNOCCUPACY_DETECTOR_H
#define TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_TRACK_UNOCCUPACY_DETECTOR_H

#include "TracksideObject.h"
#include "TrackUnoccupancy.h"
#include "TrainbrainsModulesConfig.h"

class TrackUnoccupancyDetector : public TracksideObject
{

public:

#if ALLOW_USING_LAMBDAS == USING_LAMBDAS_ENABLED
    typedef std::function<void(TrackUnoccupancyDetector *)> OnTrackUnoccupancyChanged;
#else
    typedef void (*OnTrackUnoccupancyChanged)(TrackUnoccupancyDetector *);
#endif

    volatile TrackUnoccupancy state;

    TrackUnoccupancyDetector(uint8_t anAddress, String objectId) : TracksideObject(TracksideObjectType::TRACKSIDE_OBJECT_TYPE_TRACK_UNOCCUPANCY_DETECTOR, anAddress, DEFAULT_TRACKSIDE_DEVICE_CHANNEL, objectId)
    {
        state = TrackUnoccupancy::TRACK_UNOCCUPANCY_UNKNOWN;
    }

    TrackUnoccupancyDetector(uint8_t anAddress, uint8_t aChannel, String objectId) : TracksideObject(TracksideObjectType::TRACKSIDE_OBJECT_TYPE_TRACK_UNOCCUPANCY_DETECTOR, anAddress, aChannel, objectId)
    {
        state = TrackUnoccupancy::TRACK_UNOCCUPANCY_UNKNOWN;
    }

    TrackUnoccupancyDetector(const TrackUnoccupancyDetector &other);

    ~TrackUnoccupancyDetector()
    {
        onTrackReleasedHandler = NULL;
        onTrackOccupiedHandler = NULL;
        onTrackUnoccupancyChangedHandler = NULL;
    }

    bool notifyStateChanged(TrackUnoccupancy actualTrackUnoccupancy)
    {
        if (actualTrackUnoccupancy == state)
        {
            return false;
        }

        state = actualTrackUnoccupancy;
        bool isHandled = false;

        if (onTrackUnoccupancyChangedHandler != nullptr)
        {
            onTrackUnoccupancyChangedHandler(this);
            isHandled |= true;
        }

        if (onTrackReleasedHandler != nullptr && state == TrackUnoccupancy::TRACK_UNOCCUPIED)
        {
            onTrackReleasedHandler(this);
            isHandled |= true;
        }

        if (onTrackOccupiedHandler != nullptr && state == TrackUnoccupancy::TRACK_OCCUPIED)
        {
            onTrackOccupiedHandler(this);
            isHandled |= true;
        }

        return isHandled;
    }

    void whenUnoccupancyChanged(OnTrackUnoccupancyChanged aHandler)
    {
        onTrackUnoccupancyChangedHandler = aHandler;
    }

    void whenOccupied(OnTrackUnoccupancyChanged aHandler)
    {
        onTrackOccupiedHandler = aHandler;
    }

    void whenReleased(OnTrackUnoccupancyChanged aHandler)
    {
        onTrackReleasedHandler = aHandler;
    }

    inline bool isOccupied()
    {
        return state == TrackUnoccupancy::TRACK_OCCUPIED;
    }

    inline bool isUnoccupied()
    {
        return state == TrackUnoccupancy::TRACK_UNOCCUPIED;
    }

    inline bool isInUnknownPosition()
    {
        return state == TrackUnoccupancy::TRACK_UNOCCUPANCY_UNKNOWN;
    }

private:
    OnTrackUnoccupancyChanged onTrackUnoccupancyChangedHandler = NULL;
    OnTrackUnoccupancyChanged onTrackReleasedHandler = NULL;
    OnTrackUnoccupancyChanged onTrackOccupiedHandler = NULL;
};

typedef TrackUnoccupancyDetector *TrackUnoccupancyDetectorRef;

#endif // TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_TRACK_UNOCCUPACY_DETECTOR_H
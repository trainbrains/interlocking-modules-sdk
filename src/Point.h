/*
 * Copyright (c) 2024 https://www.trainbrains.eu (Greg Barski).
 * This product is licensed under an GNU GPL, see the LICENSE file in the top-level directory.
 */

#ifndef TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_POINT_H
#define TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_POINT_H

#include "TracksideObject.h"
#include "PointPosition.h"
#include "TrainbrainsModulesConfig.h"

class Point : public TracksideObject
{

public:
#if ALLOW_USING_LAMBDAS == USING_LAMBDAS_ENABLED
    typedef std::function<void(Point *)> OnPointSwitched;
#else
    typedef void (*OnPointSwitched)(Point *);
#endif

    volatile PointPosition state;
    String coupledPointId = "";

    Point(uint8_t anAddress, String anId) : TracksideObject(TracksideObjectType::TRACKSIDE_OBJECT_TYPE_POINT, anAddress, DEFAULT_TRACKSIDE_DEVICE_CHANNEL, anId)
    {
        state = PointPosition::POSITION_UNKNOWN;
    }

    Point(uint8_t anAddress, String anId, String anCoupledPointId) : TracksideObject(TracksideObjectType::TRACKSIDE_OBJECT_TYPE_POINT, anAddress, DEFAULT_TRACKSIDE_DEVICE_CHANNEL, anId)
    {
        state = PointPosition::POSITION_UNKNOWN;
        coupledPointId = anCoupledPointId;
    }

    Point(uint8_t anAddress, uint8_t aChannel, String anId) : TracksideObject(TracksideObjectType::TRACKSIDE_OBJECT_TYPE_POINT, anAddress, aChannel, anId)
    {
        state = PointPosition::POSITION_UNKNOWN;
    }

    Point(uint8_t anAddress, uint8_t aChannel, String anId, String anCoupledPointId) : TracksideObject(TracksideObjectType::TRACKSIDE_OBJECT_TYPE_POINT, anAddress, aChannel, anId)
    {
        state = PointPosition::POSITION_UNKNOWN;
        coupledPointId = anCoupledPointId;
    }

    Point(const Point &other);

    ~Point()
    {
        onPointSwitchedHandler = NULL;
    }

    bool notifyStateChanged(PointPosition actualPointPosition)
    {
        if (actualPointPosition == state)
        {
            return false;
        }

        state = actualPointPosition;

        if (onPointSwitchedHandler != nullptr)
        {
            onPointSwitchedHandler(this);
            return true;
        }

        return false;
    }

    void whenSwitched(OnPointSwitched aHandler)
    {
        onPointSwitchedHandler = aHandler;
    }

    inline bool isInPlusPosition()
    {
        return state == PointPosition::POSITION_PLUS;
    }

    inline bool isInMinusPosition()
    {
        return state == PointPosition::POSITION_MINUS;
    }

    inline bool isInUnknownPosition()
    {
        return state == PointPosition::POSITION_UNKNOWN;
    }

private:
    OnPointSwitched onPointSwitchedHandler = NULL;
};

typedef Point *PointRef;

#endif // TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_POINT_H
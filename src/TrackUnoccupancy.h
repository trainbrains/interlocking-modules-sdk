/*
 * Copyright (c) 2024 https://www.trainbrains.eu (Greg Barski).
 * This product is licensed under an GNU GPL, see the LICENSE file in the top-level directory.
 */

#ifndef TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_TRACK_OCCUPANCY_H
#define TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_TRACK_OCCUPANCY_H

#include <Arduino.h>

enum TrackUnoccupancy
{
    TRACK_UNOCCUPANCY_UNKNOWN = 0,
    TRACK_OCCUPIED = 1,
    TRACK_UNOCCUPIED = 2
};

static String nameOf(TrackUnoccupancy trackOccupancy)
{
    switch (trackOccupancy)
    {
    case TrackUnoccupancy::TRACK_UNOCCUPANCY_UNKNOWN:
        return "unknown";
    case TrackUnoccupancy::TRACK_OCCUPIED:
        return "occupied";
    case TrackUnoccupancy::TRACK_UNOCCUPIED:
        return "unoccupied";    
    default:
        return "";
    };
}

#endif // TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_TRACK_OCCUPANCY_H
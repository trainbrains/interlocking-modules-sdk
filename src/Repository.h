/*
 * Copyright (c) 2024 https://www.trainbrains.eu (Greg Barski).
 * This product is licensed under an GNU GPL, see the LICENSE file in the top-level directory.
 */

#ifndef TRAINBRAINS_INTERLOCKING_MODULES_INFRASTRUCTURE_REPOSITORY
#define TRAINBRAINS_INTERLOCKING_MODULES_INFRASTRUCTURE_REPOSITORY

#define REPOSITORY_CAPACITY 32

template <class T>
class Repository
{

private:
    byte nextItemIndex = 0;
    byte capacity = 0;
    byte numberOfItems = 0;
    volatile T *items;

public:
    Repository(byte aCapacity)
    {
        capacity = aCapacity;
        items = new T[aCapacity];
        numberOfItems = 0;

        for (int i = 0; i < capacity; i++)
        {
            items[i] = NULL;
        }
    }

    Repository(const Repository &other);

    ~Repository()
    {
        deleteAll();
        delete (items);
        items = NULL;
    }

    bool add(T item)
    {
        if (numberOfItems >= capacity)
        {
            return false;
        }

        items[numberOfItems] = item;
        numberOfItems++;
        return true;
    }

    byte size() { return numberOfItems; }

    bool isEmpty() { return numberOfItems == 0; }

    T getNext()
    {
        if (capacity == 0)
            return NULL;

        if (nextItemIndex >= numberOfItems)
        {
            nextItemIndex = 0;
        }

        T item = items[nextItemIndex];
        nextItemIndex++;
        return item;
    }

    bool hasNext() { return nextItemIndex < numberOfItems && items[nextItemIndex] != NULL; }

    void reset() { nextItemIndex = 0; };

    void deleteAll()
    {
        reset();
        while (hasNext())
        {
            T item = getNext();
            delete (item);
            item = NULL;
        }
        numberOfItems = 0;
    }

    void clearAll()
    {
        reset();
        while (hasNext())
        {
            T item = getNext();
            // delete(item);
            item = NULL;
        }
        numberOfItems = 0;
    }
};

#endif
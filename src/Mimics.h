
#ifndef MIMICS_H
#define MIMICS_H

#include "PointPosition.h"
#include "SignalAspect.h"
#include "TrackUnoccupancy.h"

enum Mimics
{
    MIMIC_UNKNOWN = 0,
    MIMIC_CONTROL = 1,

    MIMIC_SIGNAL_STOP = 10,
    MIMIC_SIGNAL_CLEAR = 11,
    MIMIC_SIGNAL_SHUNTING_ALLOWED = 12,
    MIMIC_SIGNAL_PROCEED_ON_SIGHT_AUTHORITY = 13,
    MIMIC_SIGNAL_DARK = 14,

    MIMIC_POINT_POSITION_PLUS = 20,
    MIMIC_POINT_POSITION_MINUS = 21,
    MIMIC_POINT_POSITION_UNKNOWN = 22,
    MIMIC_POINT_POSITION_OUT_OF_CONTROL = 23,

    MIMIC_ROUTE_CLEARED = 30,
    MIMIC_ROUTE_RELEASED = 31,
    MIMIC_ROUTE_TRACK_OCCUPIED = 32,
    MIMIC_ROUTE_UNKNOWN = 33,

    MIMIC_TRACK_LOCKED = 40,
    MIMIC_TRACK_OCCUPIED = 41,
    MIMIC_TRACK_UNOCCUPIED = 42,
    MIMIC_TRACK_UNKNOWN_OCCUPANCY = 43,

    MIMIC_BUTTON_LOCKED = 50,
    MIMIC_BUTTON_UNLOCKED = 51,

    MIMIC_CROSSING_CLOSED = 60,
    MIMIC_CROSSING_OPEN = 61,
    MIMIC_CROSSING_FAULT = 62,
    MIMIC_CROSSING_DARK = 63,

    MIMIC_LINE_BLOCKING_IDLE = 69,
    MIMIC_LINE_BLOCKING_ARRIVAL_REQUESTED = 70,
    MIMIC_LINE_BLOCKING_ARRIVAL = 71,
    MIMIC_LINE_BLOCKING_ARRIVAL_SIGNAL_CLEARED = 72,
    MIMIC_LINE_BLOCKING_ARRIVAL_LINE_OCCUPIED = 73,    
    MIMIC_LINE_BLOCKING_ARRIVAL_LINE_RELEASED = 74,    
    MIMIC_LINE_BLOCKING_DEPARTURE_REQUESTED = 75,
    MIMIC_LINE_BLOCKING_DEPARTURE = 76,
    MIMIC_LINE_BLOCKING_DEPARTURE_SIGNAL_CLEARED = 77,
    MIMIC_LINE_BLOCKING_DEPARTURE_LINE_OCCUPIED = 78,    
    MIMIC_LINE_BLOCKING_DEPARTURE_LINE_RELEASED = 79    
    
};

static Mimics mimicOf(PointPosition pointPosition)
{
    switch (pointPosition)
    {
    case PointPosition::POSITION_UNKNOWN:
        return Mimics::MIMIC_POINT_POSITION_UNKNOWN;
    case PointPosition::POSITION_PLUS:
        return Mimics::MIMIC_POINT_POSITION_PLUS;
    case PointPosition::POSITION_MINUS:
        return Mimics::MIMIC_POINT_POSITION_MINUS;
    case PointPosition::POSITION_BROKEN:
        return Mimics::MIMIC_POINT_POSITION_OUT_OF_CONTROL;
    default:
        return Mimics::MIMIC_POINT_POSITION_UNKNOWN;
    }
}

static Mimics mimicOf(SignalType signalType)
{
    switch (signalType)
    {
    case SignalType::STOP:
        return Mimics::MIMIC_SIGNAL_STOP;
    case SignalType::CLEAR:
        return Mimics::MIMIC_SIGNAL_CLEAR;
    case SignalType::SHUNTING_ALLOWED:
        return Mimics::MIMIC_SIGNAL_SHUNTING_ALLOWED;
    case SignalType::PROCEED_ON_SIGHT_AUTHORITY:
        return Mimics::MIMIC_SIGNAL_PROCEED_ON_SIGHT_AUTHORITY;
    case SignalType::DARK:
        return Mimics::MIMIC_SIGNAL_DARK;
    default:
        return Mimics::MIMIC_SIGNAL_DARK;
    }
}

static Mimics mimicOf(TrackUnoccupancy trackUnoccupancy)
{
    switch (trackUnoccupancy)
    {
    case TrackUnoccupancy::TRACK_OCCUPIED:
        return Mimics::MIMIC_TRACK_OCCUPIED;
    case TrackUnoccupancy::TRACK_UNOCCUPIED:
        return Mimics::MIMIC_TRACK_UNOCCUPIED;
    case TrackUnoccupancy::TRACK_UNOCCUPANCY_UNKNOWN:
         return Mimics::MIMIC_TRACK_UNKNOWN_OCCUPANCY;
    default:
        return Mimics::MIMIC_TRACK_UNOCCUPIED;
    }
}

#endif //MIMICS_H
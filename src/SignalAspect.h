/*
 * Copyright (c) 2024 https://www.trainbrains.eu (Greg Barski).
 * This product is licensed under an GNU GPL, see the LICENSE file in the top-level directory.
 */

#ifndef TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_SIGNAL_ASPECT_H
#define TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_SIGNAL_ASPECT_H

#include <Arduino.h>

enum SignalType
{
    CONTROL = 0,
    STOP = 1,
    CLEAR = 2,
    SHUNTING_ALLOWED = 3,
    PROCEED_ON_SIGHT_AUTHORITY = 4,
    DARK = 5,
    UNSURE = 6
};

static String nameOf(SignalType state)
{
    switch (state)
    {
    case SignalType::CONTROL:
        return "control";
    case SignalType::DARK:
        return "dark";
    case SignalType::CLEAR:
        return "clear";
    case SignalType::PROCEED_ON_SIGHT_AUTHORITY:
        return "Proceed on sight authority";
    case SignalType::SHUNTING_ALLOWED:
        return "Shunting allowed";
    case SignalType::STOP:
        return "Stop!";
    case SignalType::UNSURE:
        return "Unsure";
    default:
        return "";
    };
}

class SignalAspect
{

private:
    /* data */

public:
    SignalAspect(uint8_t aLampsMask, uint8_t aBlinkMask, SignalType aSignalType)
    {
        lampsMask = aLampsMask;
        blinkMask = aBlinkMask;
        signalType = aSignalType;
    }

    volatile uint8_t lampsMask = 0;
    volatile uint8_t blinkMask = 0;
    SignalType signalType = SignalType::CLEAR;
};

static SignalAspect *SignalAspectDARK = new SignalAspect(0, 0, SignalType::DARK);

#endif // TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_SIGNAL_ASPECT_H

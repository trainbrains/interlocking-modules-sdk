/*
 * Copyright (c) 2024 https://www.trainbrains.eu (Greg Barski).
 * This product is licensed under an GNU GPL, see the LICENSE file in the top-level directory.
 */

#include "TrainbrainsModules.h"

bool TrainbrainsModules::commandIsAccepted(MessageFrame message)
{
    return message[1] == 4 && message[6] == 0;
}

uint8_t TrainbrainsModules::addressDevice(TracksideObjectRef tracksideDevice)
{
    if (isBusy)
    {
        return 0;
    }

    isBusy = true;
    uint8_t result = sendFrame(addressDeviceFrame(tracksideDevice->i2cAddress));
    isBusy = false;
    return result;
}

MessageFrame TrainbrainsModules::switchPointPosition(PointRef point)
{
    PointPosition expectedPosition = point->state != PointPosition::POSITION_PLUS ? PointPosition::POSITION_PLUS : PointPosition::POSITION_MINUS;
    return switchPointPosition(point, expectedPosition);
}

MessageFrame TrainbrainsModules::switchPointPosition(PointRef point, PointPosition expectedPosition)
{
    if (isBusy)
    {
        return EMPTY_MESSAGE_FRAME;
    }

    isBusy = true;
    if (sendFrame(switchPointFrame(point->i2cAddress, point->channel, expectedPosition)) > 0)
    {
        isBusy = false;
        return EMPTY_MESSAGE_FRAME;
    }
    MessageFrame result = awaitResultOrAssumeAcknowledge(point->i2cAddress);
    isBusy = false;
    if (result[0] == point->i2cAddress && result[1] == 4 && result[6] == 0)
    {
        point->notifyStateChanged(PointPosition::POSITION_UNKNOWN);
    }

    return result;
}

MessageFrame TrainbrainsModules::adjustPointSwitchingTime(PointRef point, uint8_t expectedSwitchingTimeInSeconds = 2)
{
    if (isBusy)
    {
        return EMPTY_MESSAGE_FRAME;
    }

    isBusy = true;
    if (sendFrame(configureDeviceFrame(point->i2cAddress, 2, expectedSwitchingTimeInSeconds)) > 0)
    {
        isBusy = false;
        return EMPTY_MESSAGE_FRAME;
    }
    MessageFrame result = awaitResultOrAssumeAcknowledge(point->i2cAddress);
    isBusy = false;
    return result;
}

MessageFrame TrainbrainsModules::checkDeviceState(uint8_t address, uint8_t channel)
{
    if (isBusy)
    {
        return EMPTY_MESSAGE_FRAME;
    }

    isBusy = true;
    if (sendFrame(checkStateFrame(address, channel)) > 0)
    {
        isBusy = false;
        return EMPTY_MESSAGE_FRAME;
    }
    MessageFrame result = awaitResult(address);
    isBusy = false;
    return result;
}

PointPosition TrainbrainsModules::checkDeviceState(PointRef point)
{
    if (isBusy)
    {
        return PointPosition::POSITION_UNKNOWN;
    }

    isBusy = true;
    if (sendFrame(checkStateFrame(point->i2cAddress, point->channel)) > 0)
    {
        isBusy = false;
        return PointPosition::POSITION_UNKNOWN;
    }
    MessageFrame result = awaitResult(point->i2cAddress);
    isBusy = false;
    if (result[0] == point->i2cAddress && result[1] == 14 && result[6] <= PointPosition::POSITION_BROKEN)
    {
        PointPosition actualState = (PointPosition)result[6];
        point->notifyStateChanged(actualState);
        return actualState;
    }
    else
    {
        PointPosition actualState = PointPosition::POSITION_UNKNOWN;
        point->notifyStateChanged(actualState);
        return actualState;
    }
}

SignalType TrainbrainsModules::checkDeviceState(LightSignalRef lightSignal)
{
    if (isBusy)
    {
        return SignalType::DARK;
    }

    isBusy = true;
    if (sendFrame(checkStateFrame(lightSignal->i2cAddress, lightSignal->channel)) > 0)
    {
        isBusy = false;
        return SignalType::DARK;
    }

    MessageFrame result = awaitResult(lightSignal->i2cAddress);
    isBusy = false;
    if (result[0] == lightSignal->i2cAddress && result[1] == 14 && result[6] <= SignalType::UNSURE)
    {
        SignalType actualState = (SignalType)result[6];
        lightSignal->notifyStateChanged(actualState);
        return actualState;
    }
    else
    {
        SignalType actualState = SignalType::DARK;
        lightSignal->notifyStateChanged(actualState);
        return actualState;
    }
}

TrackUnoccupancy TrainbrainsModules::checkDeviceState(TrackUnoccupancyDetectorRef trackOccupancyDetector)
{
    if (isBusy)
    {
        return TrackUnoccupancy::TRACK_UNOCCUPANCY_UNKNOWN;
    }

    isBusy = true;

    if (sendFrame(checkStateFrame(trackOccupancyDetector->i2cAddress, trackOccupancyDetector->channel)) > 0)
    {
        isBusy = false;
        return TrackUnoccupancy::TRACK_UNOCCUPANCY_UNKNOWN;
    }

    MessageFrame result = awaitResult(trackOccupancyDetector->i2cAddress);
    isBusy = false;
    if (result[0] == trackOccupancyDetector->i2cAddress && result[1] == 14 && result[6] <= TrackUnoccupancy::TRACK_UNOCCUPIED)
    {
        TrackUnoccupancy actualState = (TrackUnoccupancy)result[6];
        trackOccupancyDetector->notifyStateChanged(actualState);
        return actualState;
    }
    else
    {
        TrackUnoccupancy actualState = TrackUnoccupancy::TRACK_UNOCCUPANCY_UNKNOWN;
        trackOccupancyDetector->notifyStateChanged(actualState);
        return actualState;
    }
}

uint8_t TrainbrainsModules::checkDeviceChannelsCount(uint8_t i2cAddress)
{
    if (isBusy)
    {
        return 0;
    }

    isBusy = true;
    if (sendFrame(getDeviceInfoFrame(i2cAddress, DeviceInfoProperty::DEVICE_INFO_PROPERTY_NUMBER_OF_CHANNELS)) > 0)
    {
        isBusy = false;
        return 0;
    }

    MessageFrame result = awaitResult(i2cAddress);
    isBusy = false;
    if (result[0] == i2cAddress && result[1] == 20)
    {
        return result[6];
    }
    else
    {
        return 0;
    }
}

TracksideDeviceType TrainbrainsModules::checkDeviceType(uint8_t i2cAddress)
{
    if (isBusy)
    {
        return TracksideDeviceType::UNKNOWN_DEVICE_TYPE;
    }

    isBusy = true;
    if (sendFrame(getDeviceInfoFrame(i2cAddress, DeviceInfoProperty::DEVICE_INFO_PROPERTY_TYPE)) > 0)
    {
        isBusy = false;
        return TracksideDeviceType::UNKNOWN_DEVICE_TYPE;
    }

    MessageFrame result = awaitResult(i2cAddress);
    isBusy = false;
    if (result[0] == i2cAddress && result[1] == 20)
    {
        return (TracksideDeviceType)result[6];
    }
    else
    {
        return TracksideDeviceType::UNKNOWN_DEVICE_TYPE;
    }
}

bool TrainbrainsModules::updateTracksideObjectState(String objectId, uint8_t newObjectState)
{
    if (tracksideObjects == NULL)
    {
        return false;
    }

    tracksideObjects->reset();
    while (tracksideObjects->hasNext())
    {

        TracksideObjectRef anObject = tracksideObjects->getNext();
        if (!anObject->getId().equalsIgnoreCase(objectId))
        {
            continue;
        }

        return updateTracksideObjectState(anObject, newObjectState);
    }

    return false;
}

bool TrainbrainsModules::updateTracksideObjectState(TracksideObjectRef tracksideObject, uint8_t newObjectState)
{
    if (tracksideObjects == NULL)
    {
        return false;
    }

    switch (tracksideObject->objectType)
    {
    case TracksideObjectType::TRACKSIDE_OBJECT_TYPE_POINT:
        return ((PointRef)tracksideObject)->notifyStateChanged((PointPosition)newObjectState);
    case TracksideObjectType::TRACKSIDE_OBJECT_TYPE_SIGNAL:
        return ((LightSignalRef)tracksideObject)->notifyStateChanged((SignalType)newObjectState);
    case TracksideObjectType::TRACKSIDE_OBJECT_TYPE_TRACK_UNOCCUPANCY_DETECTOR:
        return ((TrackUnoccupancyDetectorRef)tracksideObject)->notifyStateChanged((TrackUnoccupancy)newObjectState);
    default:
        return false;
    }
}

MessageFrame TrainbrainsModules::setSignalAspectIndex(LightSignalRef lightSignal, uint8_t signalIndex)
{
    if (isBusy)
    {
        return EMPTY_MESSAGE_FRAME;
    }

    isBusy = true;

    if (sendFrame(setSignalAspectIndexFrame(lightSignal->i2cAddress, lightSignal->channel, signalIndex)) > 0)
    {
        isBusy = false;
        return EMPTY_MESSAGE_FRAME;
    }
    MessageFrame result = awaitResultOrAssumeAcknowledge(lightSignal->i2cAddress);
    isBusy = false;
    return result;
}

MessageFrame TrainbrainsModules::setSignalAspect(LightSignalRef lightSignal, SignalAspect signalAspect)
{
    if (isBusy)
    {
        return EMPTY_MESSAGE_FRAME;
    }

    isBusy = true;

    if (sendFrame(setSignalAspectFrame(lightSignal->i2cAddress, lightSignal->channel, signalAspect.lampsMask, signalAspect.blinkMask, signalAspect.signalType)) > 0)
    {
        isBusy = false;
        return EMPTY_MESSAGE_FRAME;
    }
    MessageFrame result = awaitResultOrAssumeAcknowledge(lightSignal->i2cAddress);
    isBusy = false;
    return result;
}

MessageFrame TrainbrainsModules::displayMimic(ControlPanelRef controlPanel, Mimics mimic)
{
    return displayMimic(controlPanel->i2cAddress, controlPanel->channel, mimic);
}

MessageFrame TrainbrainsModules::displayMimic(ControlPanelRef controlPanel, PointPosition pointPosition)
{
    return displayMimic(controlPanel->i2cAddress, controlPanel->channel, mimicOf(pointPosition));
}

MessageFrame TrainbrainsModules::displayMimic(ControlPanelRef controlPanel, SignalType signalType)
{
    return displayMimic(controlPanel->i2cAddress, controlPanel->channel, mimicOf(signalType));
}

MessageFrame TrainbrainsModules::displayMimic(ControlPanelRef controlPanel, TrackUnoccupancy trackUnoccupancy)
{
    return displayMimic(controlPanel->i2cAddress, controlPanel->channel, mimicOf(trackUnoccupancy));
}

MessageFrame TrainbrainsModules::displayMimic(uint8_t i2cAddress, uint8_t channel, PointPosition pointPosition)
{
    return displayMimic(i2cAddress, channel, mimicOf(pointPosition));
}

MessageFrame TrainbrainsModules::displayMimic(uint8_t i2cAddress, uint8_t channel, SignalType signalType)
{
    return displayMimic(i2cAddress, channel, mimicOf(signalType));
}

MessageFrame TrainbrainsModules::displayMimic(uint8_t i2cAddress, uint8_t channel, TrackUnoccupancy trackUnoccupancy)
{
    return displayMimic(i2cAddress, channel, mimicOf(trackUnoccupancy));
}

MessageFrame TrainbrainsModules::displayMimic(uint8_t i2cAddress, uint8_t channel, Mimics mimic)
{
    if (isBusy)
    {
        return EMPTY_MESSAGE_FRAME;
    }

    isBusy = true;

    if (sendFrame(setDisplayMimicFrame(i2cAddress, channel, mimic)) > 0)
    {
        isBusy = false;
        return EMPTY_MESSAGE_FRAME;
    }

    MessageFrame result = awaitResultOrAssumeAcknowledge(i2cAddress);
    isBusy = false;
    return result;
}

bool TrainbrainsModules::use(TracksideObjectRef tracksideObject)
{
    if (tracksideObjects == NULL)
    {
        return false;
    }

    return tracksideObjects->add(tracksideObject);
}

bool TrainbrainsModules::use(ControlDesktopRef controlDesktop)
{
    return controlDesktops->add(controlDesktop);
}

bool TrainbrainsModules::checkUsedObjectsState(unsigned long interval)
{
    if (tracksideObjects == NULL)
    {
        return false;
    }

    volatile unsigned long currentTime = millis();

    if (currentTime > nextTracksideObjectsStateCheck && !tracksideObjects->isEmpty())
    {
        TracksideObjectRef anObject = tracksideObjects->getNext();

        switch (anObject->objectType)
        {
        case TracksideObjectType::TRACKSIDE_OBJECT_TYPE_POINT:
            checkDeviceState((PointRef)anObject);
            break;
        case TracksideObjectType::TRACKSIDE_OBJECT_TYPE_SIGNAL:
            checkDeviceState((LightSignalRef)anObject);
            break;
        case TracksideObjectType::TRACKSIDE_OBJECT_TYPE_TRACK_UNOCCUPANCY_DETECTOR:
            checkDeviceState((TrackUnoccupancyDetectorRef)anObject);
            break;
        default:
            break;
        }

        nextTracksideObjectsStateCheck = currentTime + (interval / (tracksideObjects->size() + 1));

        return true;
    }

    return false;
}

ButtonFunction TrainbrainsModules::checkPanelButtons(ControlPanelRef panel)
{
    if (isBusy)
    {
        return ButtonFunction::BUTTON_FUNCTION_NONE;
    }

    isBusy = true;
    if (sendFrame(checkStateFrame(panel->i2cAddress, panel->channel)) > 0)
    {
        isBusy = false;
        return ButtonFunction::BUTTON_FUNCTION_NONE;
    }

    MessageFrame result = awaitResult(panel->i2cAddress);
    isBusy = false;
    if (result[0] == panel->i2cAddress && result[1] == 14)
    {
        uint8_t channel = result[6];
        ButtonFunction button = (ButtonFunction)result[7];
        panel->notifyButtonPressed(button);

        return button;
    }
    else
    {
        panel->notifyButtonReleased();
        return ButtonFunction::BUTTON_FUNCTION_NONE;
    }
}

bool TrainbrainsModules::checkUsedControlDesktops(unsigned long interval)
{
    if (controlDesktops == NULL)
    {
        return false;
    }

    volatile unsigned long currentTime = millis();

    if (currentTime > nextControlPanelButtonsCheck && !controlDesktops->isEmpty())
    {
        ControlDesktopRef aControlDesktop = controlDesktops->getNext();

        if (aControlDesktop != NULL)
        {
            checkDesktopPanelButtons(aControlDesktop) != ButtonFunction::BUTTON_FUNCTION_NONE;
        }

        nextControlPanelButtonsCheck = currentTime + interval;
        return true;
    }
    return false;
}

ButtonFunction TrainbrainsModules::checkDesktopPanelButtons(ControlDesktopRef desktop)
{
    if (isBusy)
    {
        return ButtonFunction::BUTTON_FUNCTION_NONE;
    }

    isBusy = true;
    if (sendFrame(checkStateFrame(desktop->i2cAddress, 0)) > 0)
    {
        isBusy = false;
        return ButtonFunction::BUTTON_FUNCTION_NONE;
    }

    MessageFrame result = awaitResult(desktop->i2cAddress);
    isBusy = false;
    if (result[0] == desktop->i2cAddress && result[1] == 14)
    {
        uint8_t channel = result[6];
        ButtonFunction button = (ButtonFunction)result[7];
        if (button == ButtonFunction::BUTTON_FUNCTION_NONE)
        {
            desktop->notifyButtonsReleased();
        }
        else
            desktop->notifyButtonPressed(channel, button);

        return button;
    }
    else
    {
        return ButtonFunction::BUTTON_FUNCTION_NONE;
    }
}

uint8_t TrainbrainsModules::getTracksideObjectState(String objectId)
{
    if (tracksideObjects == NULL)
    {
        return 0;
    }

    tracksideObjects->reset();
    while (tracksideObjects->hasNext())
    {
        TracksideObjectRef anObject = tracksideObjects->getNext();
        if (!anObject->getId().equalsIgnoreCase(objectId))
        {
            continue;
        }

        switch (anObject->objectType)
        {
        case TracksideObjectType::TRACKSIDE_OBJECT_TYPE_SIGNAL:
            return (LightSignalRef(anObject))->state;
        case TracksideObjectType::TRACKSIDE_OBJECT_TYPE_DISTANT_SIGNAL:
            return (LightSignalRef(anObject))->state;
        case TracksideObjectType::TRACKSIDE_OBJECT_TYPE_POINT:
            return (PointRef(anObject))->state;
        case TracksideObjectType::TRACKSIDE_OBJECT_TYPE_TRACK_UNOCCUPANCY_DETECTOR:
            return (TrackUnoccupancyDetectorRef(anObject))->state;
        default:
            return 0;
        }
    }

    return 0;
}

void TrainbrainsModules::removeAll()
{
    nextControlDesktopUpdate = 0UL;
    nextControlPanelButtonsCheck = 0UL;
    nextTracksideObjectsStateCheck = 0UL;
    delete tracksideObjects;
    tracksideObjects = NULL;

    delete controlDesktops;
    controlDesktops = NULL;
}

void TrainbrainsModules::setup(int maxNumberOfTracksideObjects, int maxNumberOfControlDesktopsObjects)
{
    tracksideObjects = new Repository<TracksideObjectRef>(maxNumberOfTracksideObjects);
    controlDesktops = new Repository<ControlDesktopRef>(maxNumberOfControlDesktopsObjects);
}

void TrainbrainsModules::init()
{
    i2c->begin();
}
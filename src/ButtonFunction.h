/*
 * Copyright (c) 2024 https://www.trainbrains.eu (Greg Barski).
 * This product is licensed under an GNU GPL, see the LICENSE file in the top-level directory.
 */

#ifndef TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_BUTTON_FUNCTION_H
#define TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_BUTTON_FUNCTION_H

enum ButtonFunction
{
    BUTTON_FUNCTION_NONE = 0,

    BUTTON_FUNCTION_ROUTE_RESTORE = 1,

    BUTTON_FUNCTION_SIGNAL_CLEAR = 10,
    BUTTON_FUNCTION_SIGNAL_SHUNTING = 11,
    BUTTON_FUNCTION_SIGNAL_POSA = 12,

    BUTTON_FUNCTION_POINT_SWITCH = 20,

    BUTTON_FUNCTION_OPERATE_DEPARTURE_BLOCK = 30,
    BUTTON_FUNCTION_OPERATE_DEPARTURE_BLOCK_INTERIMLY = 31,
    BUTTON_FUNCTION_OPERATE_ARRIVAL_BLOCK = 32,
    BUTTON_FUNCTION_OPERATE_ARRIVAL_BLOCK_INTERIMLY = 33,
    BUTTON_FUNCTION_OPERATE_ALLOW_ARRIVAL_BLOCK = 34

};

static String nameOf(ButtonFunction function)
{
    switch (function)
    {
    case ButtonFunction::BUTTON_FUNCTION_POINT_SWITCH:
        "Switch point";
    case ButtonFunction::BUTTON_FUNCTION_ROUTE_RESTORE:
        "Restore route from";
    case ButtonFunction::BUTTON_FUNCTION_SIGNAL_CLEAR:
        "Clear signal";
    case ButtonFunction::BUTTON_FUNCTION_SIGNAL_POSA:
        "Allow POSA from";
    case ButtonFunction::BUTTON_FUNCTION_SIGNAL_SHUNTING:
        "Allow shunting from";
    case ButtonFunction::BUTTON_FUNCTION_OPERATE_DEPARTURE_BLOCK:
        "Operate departure block";
    case ButtonFunction::BUTTON_FUNCTION_OPERATE_DEPARTURE_BLOCK_INTERIMLY:
        "Operate departure block interimly";
    case ButtonFunction::BUTTON_FUNCTION_OPERATE_ARRIVAL_BLOCK:
        "Operate arrival block";
    case ButtonFunction::BUTTON_FUNCTION_OPERATE_ARRIVAL_BLOCK_INTERIMLY:
        "Operate arrival block interimly";
    case ButtonFunction::BUTTON_FUNCTION_OPERATE_ALLOW_ARRIVAL_BLOCK:
        "Operate allow arrival block";
    default:
        return "";
    }
}

#endif // TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_BUTTON_FUNCTION_H
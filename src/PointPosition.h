/*
 * Copyright (c) 2024 https://www.trainbrains.eu (Greg Barski).
 * This product is licensed under an GNU GPL, see the LICENSE file in the top-level directory.
 */

#ifndef TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_POINT_POSITION_H
#define TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_POINT_POSITION_H

#include <Arduino.h>

enum PointPosition
{
    POSITION_UNKNOWN = 0,
    POSITION_PLUS = 1,
    POSITION_MINUS = 2,
    POSITION_BROKEN = 3
};

static String nameOf(PointPosition pointPosition)
{
    switch (pointPosition)
    {
    case PointPosition::POSITION_UNKNOWN:
        return "unknown";
    case PointPosition::POSITION_MINUS:
        return "minus";
    case PointPosition::POSITION_PLUS:
        return "plus";
    case PointPosition::POSITION_BROKEN:
        return "broken";
    default:
        return "";
    };
}

#endif // TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_POINT_POSITION_H
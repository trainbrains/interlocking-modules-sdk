/*
 * Copyright (c) 2024 https://www.trainbrains.eu (Greg Barski).
 * This product is licensed under an GNU GPL, see the LICENSE file in the top-level directory.
 */

#ifndef TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_CONTROL_DESKTOP_2203_H
#define TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_CONTROL_DESKTOP_2203_H

#include "PointPosition.h"
#include "ButtonFunction.h"
#include "ControlDesktop.h"
#include "ControlPanel.h"

class ControlDesktop2203 : ControlDesktop
{
#define CONTROL_DESKTOP_2203_NUMBER_OF_CHANNELS 11
public:   

    ControlDesktop2203(uint8_t anAddress) : ControlDesktop(anAddress, 11)
    {
        controlPanels[0] = new ControlPanel(anAddress, 1);
        controlPanels[1] = new ControlPanel(anAddress, 2);
        controlPanels[2] = new ControlPanel(anAddress, 3);
        controlPanels[3] = new ControlPanel(anAddress, 4);
        controlPanels[4] = new ControlPanel(anAddress, 5);
        controlPanels[5] = new ControlPanel(anAddress, 6);

        controlPanels[6] = new ControlPanel(anAddress, 7);
        controlPanels[7] = new ControlPanel(anAddress, 8);
        controlPanels[8] = new ControlPanel(anAddress, 9);
        controlPanels[9] = new ControlPanel(anAddress, 10);
        controlPanels[10] = new ControlPanel(anAddress, 11);
    }

    ControlDesktop2203(const ControlDesktop2203 &other);

    ~ControlDesktop2203()
    {
        for (uint8_t i = 0; i < CONTROL_DESKTOP_2203_NUMBER_OF_CHANNELS; i++)
        {
            controlPanels[i] = NULL;
            delete (controlPanels[i]);
        }

        onButtonPressedHandler = NULL;
    }

    ControlPanelRef panel(uint8_t channel)
    {
        if (channel < 1 || channel > CONTROL_DESKTOP_2203_NUMBER_OF_CHANNELS)
            return NULL;
        return controlPanels[channel - 1];
    }

    bool notifyButtonPressed(uint8_t channel, ButtonFunction actualButtonFunction)
    {
        if (channel == 0)
        {
            state = ButtonFunction::BUTTON_FUNCTION_NONE;
            return false;
        }

        if (actualButtonFunction == state)
        {
            return false;
        }

        state = actualButtonFunction;
        bool result = false;

        if (state != ButtonFunction::BUTTON_FUNCTION_NONE)
        {

            if (panel(channel) != nullptr)
            {
                panel(channel)->notifyButtonPressed(state);
                result = true;
            }

            if (onButtonPressedHandler != nullptr)
            {
                onButtonPressedHandler(i2cAddress, channel, state);
                result = true;
            }
        }

        return result;
    }

    bool notifyButtonsReleased()
    {
        if (state != ButtonFunction::BUTTON_FUNCTION_NONE)
        {
            state = ButtonFunction::BUTTON_FUNCTION_NONE;

            for (uint8_t i = 0; i < CONTROL_DESKTOP_2203_NUMBER_OF_CHANNELS; i++)
            {
                controlPanels[i]->notifyButtonReleased();
            }

            return true;
        }
        return false;
    }

    bool isPressed() { return state != ButtonFunction::BUTTON_FUNCTION_NONE; }

    void handleButtonPressed(OnControlDesktopButtonPressed handler){
        onButtonPressedHandler = handler;
    }

private:
    OnControlDesktopButtonPressed onButtonPressedHandler = NULL;
    volatile ButtonFunction state = ButtonFunction::BUTTON_FUNCTION_NONE;
    volatile ControlPanelRef controlPanels[CONTROL_DESKTOP_2203_NUMBER_OF_CHANNELS] = {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};
};

typedef ControlDesktop2203 *ControlDesktop2203Ref;

#endif //TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_CONTROL_DESKTOP_2203_H
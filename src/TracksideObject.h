/*
 * Copyright (c) 2024 https://www.trainbrains.eu (Greg Barski).
 * This product is licensed under an GNU GPL, see the LICENSE file in the top-level directory.
 */

#ifndef TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_TRACKSIDE_OBJECT_H
#define TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_TRACKSIDE_OBJECT_H

#include "TracksideObjectType.h"

#define DEFAULT_TRACKSIDE_DEVICE_CHANNEL 1

class TracksideObject
{

protected:
    String id;

public:
    volatile uint8_t i2cAddress;
    volatile uint8_t channel;
    volatile TracksideObjectType objectType;    

    TracksideObject(TracksideObjectType anObjectType)
    {
        i2cAddress = 0;
        channel = 0;
        objectType = anObjectType;
        id = "";        
    }

    TracksideObject(TracksideObjectType anObjectType, uint8_t anAddress, uint8_t aChannel, String anId)
    {
        objectType = anObjectType;
        i2cAddress = anAddress;
        channel = aChannel;
        id = anId;
    }

    TracksideObject(const TracksideObject &other);

    String getId() { return id; }
};

typedef TracksideObject *TracksideObjectRef;

static TracksideObjectRef NO_DEVICE = new TracksideObject(TracksideObjectType::UNKNOWN_TRACKSIDE_OBJECT_TYPE, 0, 0, "");

#endif //TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_TRACKSIDE_OBJECT_H
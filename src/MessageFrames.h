/*
 * Copyright (c) 2024 https://www.trainbrains.eu (Greg Barski).
 * This product is licensed under an GNU GPL, see the LICENSE file in the top-level directory.
 */

#ifndef TRAINBRAINS_INTERLOCKING_MODULES_INFRASTRUCTURE_MESSAGEFRAMES
#define TRAINBRAINS_INTERLOCKING_MODULES_INFRASTRUCTURE_MESSAGEFRAMES

#include <Arduino.h>


typedef uint8_t *MessageFrame;
#define MESSAGE_FRAME_LENGTH 10
static MessageFrame EMPTY_MESSAGE_FRAME = new uint8_t[MESSAGE_FRAME_LENGTH]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

static MessageFrame switchPointFrame(uint8_t address, uint8_t channel, uint8_t expectedPosition)
{
    uint8_t *frame = new uint8_t[MESSAGE_FRAME_LENGTH]{address, 12, 1, channel, 0, 0, expectedPosition, 0, 0, 0};
    return frame;
}

static MessageFrame configureDeviceFrame(uint8_t address, uint8_t cv, uint8_t expectedValue)
{
    uint8_t *frame = new uint8_t[MESSAGE_FRAME_LENGTH]{address, 6, 1, cv, 0, 0, expectedValue, 0, 0, 0};
    return frame;
}

static MessageFrame setSignalAspectFrame(uint8_t address, uint8_t channel, uint8_t signalLightsMask, uint8_t blinkLightsMask, uint8_t signalType)
{
    uint8_t *frame = new uint8_t[MESSAGE_FRAME_LENGTH]{address, 13, 1, channel, 0, 0, signalLightsMask, blinkLightsMask, signalType, 0};
    return frame;
}

static  MessageFrame setSignalAspectIndexFrame(uint8_t address, uint8_t channel, uint8_t signalIndex)
{
    uint8_t *frame = new uint8_t[MESSAGE_FRAME_LENGTH]{address, 9, 1, channel, 0, 0, signalIndex, 0, 0, 0};
    return frame;
}

static MessageFrame checkStateFrame(uint8_t address, uint8_t channel)
{
    uint8_t *frame = new uint8_t[MESSAGE_FRAME_LENGTH]{address, 14, 1, channel, 0, 0, 0, 0, 0, 0};
    return frame;
}

static  MessageFrame addressDeviceFrame(uint8_t address)
{
    uint8_t *frame = new uint8_t[MESSAGE_FRAME_LENGTH]{0, 2, 1, address, 0, 0, 0, 0, 0, 0};
    return frame;
}

static  MessageFrame getDeviceInfoFrame(uint8_t address, uint8_t property)
{
    uint8_t *frame = new uint8_t[MESSAGE_FRAME_LENGTH]{address, 20, 1, property, 0, 0, 0, 0, 0, 0};
    return frame;
}

static MessageFrame setDisplayMimicFrame(uint8_t address, uint8_t channel, uint8_t mimic)
{
    uint8_t *frame = new uint8_t[MESSAGE_FRAME_LENGTH]{address, 5, 1, channel, 0, 0, mimic, 0, 0, 0};
    return frame;
}

static MessageFrame acknowledgeFrame(uint8_t address)
{
    uint8_t *frame = new uint8_t[MESSAGE_FRAME_LENGTH]{address, 4, 1, 0, 0, 0, 0, 0, 0, 0};
    return frame;
}


#endif // TRAINBRAINS_INTERLOCKING_MODULES_INFRASTRUCTURE_MESSAGEFRAMES
/*
 * Copyright (c) 2024 https://www.trainbrains.eu (Greg Barski).
 * This product is licensed under an GNU GPL, see the LICENSE file in the top-level directory.
 */

#ifndef TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_LIGHT_SIGNAL_H
#define TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_LIGHT_SIGNAL_H

#include "TracksideObject.h"
#include "SignalAspect.h"
#include "BasicSignalAspects.h"
#include "TrainbrainsModulesConfig.h"

class LightSignal : public TracksideObject
{

public:
#if ALLOW_USING_LAMBDAS == USING_LAMBDAS_ENABLED
    typedef std::function<void(LightSignal *)> OnSignalTypeChanged;
#else
    typedef void (*OnSignalTypeChanged)(LightSignal *);
#endif

    volatile SignalType state;
    SignalAspect *defaultSignalAspect;
    String distanceSignalsNextSignalIdFor = "";

    LightSignal(uint8_t anAddress, String anId) : TracksideObject(TracksideObjectType::TRACKSIDE_OBJECT_TYPE_SIGNAL, anAddress, DEFAULT_TRACKSIDE_DEVICE_CHANNEL, anId)
    {
        state = SignalType::CONTROL;
        defaultSignalAspect = &BasicSignalAspectStop;
    }

      LightSignal(uint8_t anAddress, String anId, String anIdForDistanceSignals) : TracksideObject(TracksideObjectType::TRACKSIDE_OBJECT_TYPE_SIGNAL, anAddress, DEFAULT_TRACKSIDE_DEVICE_CHANNEL, anId)
    {
        state = SignalType::CONTROL;
        defaultSignalAspect = &BasicSignalAspectStop;
        distanceSignalsNextSignalIdFor = anIdForDistanceSignals;
    }

    LightSignal(uint8_t anAddress, uint8_t aChannel, String anId) : TracksideObject(TracksideObjectType::TRACKSIDE_OBJECT_TYPE_SIGNAL, anAddress, aChannel, anId)
    {
        state = SignalType::CONTROL;
        defaultSignalAspect = &BasicSignalAspectStop;        
    }

    LightSignal(uint8_t anAddress, uint8_t aChannel, String anId, String anIdForDistanceSignals) : TracksideObject(TracksideObjectType::TRACKSIDE_OBJECT_TYPE_SIGNAL, anAddress, aChannel, anId)
    {
        state = SignalType::CONTROL;
        defaultSignalAspect = &BasicSignalAspectStop;        
        distanceSignalsNextSignalIdFor = anIdForDistanceSignals;
    }

    LightSignal(uint8_t anAddress, SignalAspect *aDefaultSignalAspect, String anId) : TracksideObject(TracksideObjectType::TRACKSIDE_OBJECT_TYPE_SIGNAL, anAddress, DEFAULT_TRACKSIDE_DEVICE_CHANNEL, anId)
    {
        state = SignalType::CONTROL;
        defaultSignalAspect = aDefaultSignalAspect;
    }

     LightSignal(uint8_t anAddress, SignalAspect *aDefaultSignalAspect, String anId, String anIdForDistanceSignals) : TracksideObject(TracksideObjectType::TRACKSIDE_OBJECT_TYPE_SIGNAL, anAddress, DEFAULT_TRACKSIDE_DEVICE_CHANNEL, anId)
    {
        state = SignalType::CONTROL;
        defaultSignalAspect = aDefaultSignalAspect;
        distanceSignalsNextSignalIdFor = anIdForDistanceSignals;
    }

    LightSignal(uint8_t anAddress, uint8_t aChannel, SignalAspect *aDefaultSignalAspect, String anId = "") : TracksideObject(TracksideObjectType::TRACKSIDE_OBJECT_TYPE_SIGNAL, anAddress, aChannel, anId)
    {
        state = SignalType::CONTROL;
        defaultSignalAspect = aDefaultSignalAspect;
    }
     
      LightSignal(uint8_t anAddress, uint8_t aChannel, SignalAspect *aDefaultSignalAspect, String anIdForDistanceSignals, String anId = "") : TracksideObject(TracksideObjectType::TRACKSIDE_OBJECT_TYPE_SIGNAL, anAddress, aChannel, anId)
    {
        state = SignalType::CONTROL;
        defaultSignalAspect = aDefaultSignalAspect;
        distanceSignalsNextSignalIdFor = anIdForDistanceSignals;
    }

    LightSignal(const LightSignal &other);

    ~LightSignal()
    {
        onSignalTypeChangedHandler = NULL;
    }

    bool notifyStateChanged(SignalType actualSignalType)
    {
        if (actualSignalType == state)
        {
            return false;
        }

        state = actualSignalType;

        if (onSignalTypeChangedHandler != nullptr)
        {
            onSignalTypeChangedHandler(this);
            return true;
        }

        return false;
    }

    void whenSwitched(OnSignalTypeChanged aHandler)
    {
        onSignalTypeChangedHandler = aHandler;
    }

    inline bool isCleared()
    {
        return state == SignalType::CLEAR;
    }

    inline bool isStop()
    {
        return state == SignalType::STOP;
    }

    inline bool isProceedOnSightAuthority()
    {
        return state == SignalType::PROCEED_ON_SIGHT_AUTHORITY;
    }

    inline bool isShuntingAllowed()
    {
        return state == SignalType::SHUNTING_ALLOWED;
    }

private:
    OnSignalTypeChanged onSignalTypeChangedHandler = NULL;
};

typedef LightSignal *LightSignalRef;

#endif // TRAINBRAINS_INTERLOCKING_MODULES_DOMAIN_LIGHT_SIGNAL_H
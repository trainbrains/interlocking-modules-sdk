#ifndef TRACKSIDE_DEVICE_TYPE_H
#define TRACKSIDE_DEVICE_TYPE_H

#include <Arduino.h>

enum TracksideDeviceType
{
    UNKNOWN_DEVICE_TYPE = 0,
    SIGNAL_DRIVER = 1,
    POINT_DRIVER = 2,
    TRACK_POWER_DRIVER = 3,
    TRACK_INOCCUPANCY_DETECTOR = 4,
    AXEL_COUNTER = 5,
    
    LINE_BLOCK_INDICATOR_PANEL = 10,

    DESKTOP_MIMIC_INDICATOR_PANEL = 20
};

static String nameOf(TracksideDeviceType deviceType)
{
    switch (deviceType)
    {
    case TracksideDeviceType::SIGNAL_DRIVER:
        return "Signals driver";
    case TracksideDeviceType::POINT_DRIVER:
        return "Points driver";
    case TracksideDeviceType::UNKNOWN_DEVICE_TYPE:
        return "Unknown";
    case TracksideDeviceType::AXEL_COUNTER:
        return "Axels counter";
    case TracksideDeviceType::LINE_BLOCK_INDICATOR_PANEL:
        return "Line block ind. panel";
    case TracksideDeviceType::TRACK_POWER_DRIVER:
        return "Track power driver";  
    case TracksideDeviceType::TRACK_INOCCUPANCY_DETECTOR:
        return "Inoccupancy detector";
    case TracksideDeviceType::DESKTOP_MIMIC_INDICATOR_PANEL:
        return "Desktop mimic indicator";
    default:
        return "?";
    }
}

#endif // TRACKSIDE_DEVICE_TYPE_H
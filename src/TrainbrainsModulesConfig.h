/*
 * Copyright (c) 2024 https://www.trainbrains.eu (Greg Barski).
 * This product is licensed under an GNU GPL, see the LICENSE file in the top-level directory.
 */
#ifndef TRAINBRAINS_INTERLOCKING_MODULES_CONFIG
#define TRAINBRAINS_INTERLOCKING_MODULES_CONFIG

/*
* Allows to use lambdas.  Uncomment the line below to use lambdas. 
*/
#define USING_LAMBDAS_DISABLED 0
#define USING_LAMBDAS_ENABLED 1

#ifndef TRAINBRAINS_SDK_CONFIG_USING_LAMBDAS
#define ALLOW_USING_LAMBDAS USING_LAMBDAS_DISABLED
#else
#define ALLOW_USING_LAMBDAS USING_LAMBDAS_ENABLED
#endif // TRAINBRAINS_SDK_CONFIG_USING_LAMBDAS


/*
* Awaits for I2C command result. 
* By defualt it's disabled and the command result is just an static empty message.
* When enabled, it creates new MessageFrame object each time, which have to be deleted manually to aviod memory leaks.
*/
#define AWAIT_COMMAND_RESULT_DISABLED 0
#define AWAIT_COMMAND_RESULT_ENABLED 1

#ifndef TRAINBRAINS_SDK_CONFIG_AWAIT_COMMAND_RESULT
#define AWAIT_COMMAND_RESULT AWAIT_COMMAND_RESULT_DISABLED
#else
#define AWAIT_COMMAND_RESULT AWAIT_COMMAND_RESULT_ENABLED
#endif // TRAINBRAINS_SDK_CONFIG_AWAIT_COMMAND_RESULT


/*
* Aknowledgeds I2C commands without awaiting for the actual result. 
* By defualt it's disabled and the command result is just an static empty message.
* When enabled, it creates new MessageFrame object each time, which have to be deleted manually to aviod memory leaks.
*/
#define AKNOWLEDGE_I2C_COMMANDS_DISABLED 0
#define AKNOWLEDGE_I2C_COMMANDS_ENABLED 1

#ifndef TRAINBRAINS_SDK_CONFIG_AKNOWLEDGE_I2C_COMMANDS
#define AKNOWLEDGE_I2C_COMMANDS AKNOWLEDGE_I2C_COMMANDS_DISABLED
#else
#define AKNOWLEDGE_I2C_COMMANDS AKNOWLEDGE_I2C_COMMANDS_ENABLED
#endif // TRAINBRAINS_SDK_CONFIG_AKNOWLEDGE_I2C_COMMANDS





#endif // TRAINBRAINS_INTERLOCKING_MODULES_CONFIG
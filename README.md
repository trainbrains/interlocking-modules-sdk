# Trainbrains Interlocking Modules Sdk

## Getting started

Import the library to your project. You can use library manager or import it directly from this repository.

We recoment starting with the examples attached to the code. Currently the cover topics of
- addressign devices
- switching points
- switching light signals
- checking trackside device' state

## Device addressing
Each trackside device like a light signal or point have to be addressed by the driver address and channel to which it's connected.

Allowed address range is 10-127.
Alloved channel range depends on particular device model, usually 1-4.

Let's have a layout with 3 points. We will use 2 driver modules:
- Signle-channel point driver at address 20
- Double-channel point driver at address 21

The points are connected to the drives as follows:
- point 1 driver is connected to the driver on address 20, channel 1
- point 2 driver is connected to the driver on address 21, channel 1
- point 3 driver is connected to the driver on address 21, channel 2

To declate all the devices, you can do it like this.
```
#include <Arduino.h>
#include "TrainbrainsModules.h"

TrainbrainsModules *modules = new TrainbrainsModules();

PointRef point1 = new Point(20);
PointRef point2 = new Point(21, 1);
PointRef point3 = new Point(21, 2);

void setup()
{
  modules->init();
}
```

## More devices

If you plan to use more devices, you should consider declaring the number of them.

By defualt the library assumes you will be using up to 32 trackside object and up to 8 control panles.

To use more, init the modules like that:

```
#include <Arduino.h>
#include "TrainbrainsModules.h"

TrainbrainsModules *modules = new TrainbrainsModules(40, 13);  // 40 trackside objects and 13 control panles

...

void setup()
{
  modules->init();  // 40 trackside objects and 13 control panels.
}
```


## Using lambda expressions

It is supported to use lambda expessions to handle some events in dedicated classes. 

To allow using lambda expressions, you have to add following definition in a main project file just before including of trainbrains modules SDK, like:

```
#define ALLOW_USING_LAMBDAS USING_LAMBDAS_DISABLED
#include "TrainbrainsModules.h"
```

Then you can write code like below to handle changes outside the main file:
```
trackOccupancyDetector->whenOccupied([this, signal, signalAspect](TrackUnoccupancyDetectorRef trackOccupancyDetector)
                                         { modules->setSignalAspect(signalA, signalAspect); });
```


## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.


## License
This product is licensed under an GNU GPL, see the LICENSE file in the top-level directory.




/*
 * Copyright (c) 2024 https://www.trainbrains.eu (Greg Barski).
 * This product is licensed under an GNU GPL, see the LICENSE file in the top-level directory.
 */

#include <Arduino.h>
#include "TrainbrainsModules.h"

/*
 * In this example there is a layout with 3 points.
 *
 *  Infrastruture is:
 *  - Signle point driver at address 20
 *  - Double points driver at address 21
 *
 *  Points are connected to
 *  - point 1 driver is connected to the driver on address 20, channel 1
 *  - point 2 driver is connected to the driver on address 21, channel 1
 *  - point 3 driver is connected to the driver on address 21, channel 2
 */

TrainbrainsModulesRef modules = new TrainbrainsModules();

/**
 * Here you define which address you want to use for each device.
*/
PointRef point1 = new Point(20, "Point 1");    // address=20, channel=1 (default)
PointRef point2 = new Point(21, 1, "Point 2"); // address=21, channel=1
PointRef point3 = new Point(21, 2, "Point 3"); // address=21, channel=2

void setup()
{
  // Initialize modules.
  modules->init();
}

void loop()
{
  /*
   * IMPORTANT NOTE
   * 
   * To address the device:
   * 1) Uncomment single line corresponding to the device you want to address;
   * 2) Upload code to your master device;
   * 3) Press the ADDRESS button to switch the board to addressing mode;
   * 4) Await until address get changed;
   * 4) Repeate these steps to all your devices.
   * 
   * This will allow you to ensure that only one device has been assigned the expected address.
   */

  modules->addressDevice(point1);
  //modules->addressDevice(point2);
  //modules->addressDevice(point3);

  // Give it some brake as the adressing will be performed only once;
  delay(10000);
}
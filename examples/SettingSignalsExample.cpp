/*
 * Copyright (c) 2022 https://www.trainbrains.eu (Greg Barski).
 * This product is licensed under an GNU GPL, see the LICENSE file in the top-level directory.
 */

#include <Arduino.h>
#include "TrainbrainsModules.h"
#include "PKPSignalAspects.h"

/*
 * In this example there is a layout with 3 signal.
 *
 *  Infrastruture is:
 *  - Signle light driver at address 10
 *  - Signle light driver at address 11
 *  - Signle light driver at address 12
 *
 *  Light signals are connected to:
 *  - signal 1 is connected to the driver on address 10, channel 1
 *  - signal 2 is connected to the driver on address 11, channel 1
 *  - signal 3 is connected to the driver on address 12, channel 1
 */

TrainbrainsModulesRef modules = new TrainbrainsModules();
LightSignalRef signalA = new LightSignal(10, "Signal A");
LightSignalRef signalB = new LightSignal(11, "Signal B");
LightSignalRef signalC = new LightSignal(12, "Signal C");

void setup()
{
  modules->init();
  modules->setSignalAspect(signalA, SignalAspectS1);
  modules->setSignalAspect(signalB, SignalAspectS1);
  modules->setSignalAspect(signalC, SignalAspectS1);
}

void loop()
{
  // Display proceed signal on Signal A
  modules->setSignalAspect(signalC, SignalAspectS1);  //Stop
  modules->setSignalAspect(signalA, SignalAspectS12); //Proceed
  delay(4000);

  // Display proceed signal on Signal B
  modules->setSignalAspect(signalA, SignalAspectS1);  //Stop
  modules->setSignalAspect(signalB, SignalAspectS12); //Proceed
  delay(4000);

  // Display proceed signal on Signal C
  modules->setSignalAspect(signalB, SignalAspectS1);  //Stop
  modules->setSignalAspect(signalC, SignalAspectS12); //Proceed
  delay(4000);
}
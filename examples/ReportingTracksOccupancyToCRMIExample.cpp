/*
 * Copyright (c) 2022 https://www.trainbrains.eu (Greg Barski).
 * This product is licensed under an GNU GPL, see the LICENSE file in the top-level directory.
 */

#include <Arduino.h>
#include "TrainbrainsModules.h"
#include "CMRI.h"

/*
 * In this example there is a layout with 1 track unoccupancy detector.
 * We are gonna display some signals aspects on given light signal when track get occupied of released.
 *
 *  Infrastruture is:
 *  - Signle light driver at address 10
 *  - Track unoccupancy detector at address 30
 *
 *  Light signal is connected to:
 *  - signal 1 is connected to the driver on address 10, channel 1
 * 
 *  Isolated track is connected tothe detector on address 30, channel 1
 */

CMRI cmri; // defaults to a SMINI with address 0. SMINI = 24 inputs, 48 outputs

TrainbrainsModulesRef modules = new TrainbrainsModules();
TrackUnoccupancyDetectorRef track1 = new TrackUnoccupancyDetector(30, 1, "track 1");
TrackUnoccupancyDetectorRef track2 = new TrackUnoccupancyDetector(30, 2, "track 2");
TrackUnoccupancyDetectorRef track3 = new TrackUnoccupancyDetector(30, 3, "track 3");
TrackUnoccupancyDetectorRef track4 = new TrackUnoccupancyDetector(30, 4, "track 4");

void reportTrackUnoccupancy(TrackUnoccupancyDetectorRef trackOccupancyDetector){
  byte bitIndex = trackOccupancyDetector->channel-1; //Change form 1-based to 0-based values
  cmri.set_bit(bitIndex, trackOccupancyDetector->isOccupied());  
}

void setup()
{  
  Serial.begin(9600, SERIAL_8N2); // make sure this matches your speed set in JMRI

  modules->init();
  modules->use(track1);
  modules->use(track2);
  modules->use(track3);
  modules->use(track4);

  // When track occupancy change, handle new state
  track1->whenUnoccupancyChanged(reportTrackUnoccupancy);  
  track2->whenUnoccupancyChanged(reportTrackUnoccupancy);  
  track3->whenUnoccupancyChanged(reportTrackUnoccupancy);  
  track4->whenUnoccupancyChanged(reportTrackUnoccupancy);  
}

void loop()
{
  cmri.process();

  // Keep checking current state of devices
  modules->checkUsedObjectsState();
}
/*
 * Copyright (c) 2022 https://www.trainbrains.eu (Greg Barski).
 * This product is licensed under an GNU GPL, see the LICENSE file in the top-level directory.
 */

#include <Arduino.h>
#include "TrainbrainsModules.h"
#include "Point.h"

/*
 * In this example there is a layout with 1 point.
 * We are gonna switch point position and print it current state to the serial port.
 * 
 *  Infrastruture is:
 *  - Signle point driver at address 20 
 *
 *  Point1 is connected to
 *  - point 1 driver is connected to the driver on address 20, channel 1
 */

TrainbrainsModulesRef modules = new TrainbrainsModules();
PointRef point1 = new Point(20, "Point 1"); //Check the AdressingDeivesExample file

void printPointPosition();

void setup()
{
  Serial.begin(115200);
  modules->init();  
  modules->switchPointPosition(point1, PointPosition::POSITION_PLUS);
}

void loop()
{
  // Display switching point 1
  modules->switchPointPosition(point1, PointPosition::POSITION_PLUS); 
  delay(2100);
  printPointPosition();

  modules->switchPointPosition(point1, PointPosition::POSITION_MINUS);
  delay(2100);
  printPointPosition();
}

void printPointPosition()
{
  modules->checkDeviceState(point1);
  Serial.println("Point 1 position: " + nameOf(point1->state));
}
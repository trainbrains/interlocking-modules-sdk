/*
 * Copyright (c) 2022 https://www.trainbrains.eu (Greg Barski).
 * This product is licensed under an GNU GPL, see the LICENSE file in the top-level directory.
 */

#include <Arduino.h>
#include "TrainbrainsModules.h"
#include "PKPSignalAspects.h"

/*
 * In this example there is a layout with 1 signal.
 * We are gonna display some signals aspects on given light signal
 * and print it current state to the serial port.
 *
 *  Infrastruture is:
 *  - Signle light driver at address 10
 *
 *  Light signal is connected to:
 *  - signal 1 is connected to the driver on address 10, channel 1
 */

TrainbrainsModulesRef modules = new TrainbrainsModules();
LightSignalRef signalA = new LightSignal(10, "Signal A"); //Check the AdressingDeivesExample file;

void printSignalsState();

void setup()
{
  Serial.begin(115200);
  modules->init();
  modules->setSignalAspect(signalA, SignalAspectS1);
}

void loop()
{
  // Display proceed signal on Signal A and display current state
  modules->setSignalAspect(signalA, SignalAspectS1); //Stop
  printSignalsState();
  delay(2000);

  // Display proceed signal on Signal A and display current state
  modules->setSignalAspect(signalA, SignalAspectS2); //Clear
  printSignalsState();
  delay(2000);

  // Display proceed signal on Signal A and display current state
  modules->setSignalAspect(signalA, SignalAspectM2); //Shunting allowed
  printSignalsState();
  delay(2000);

  // Display proceed signal on Signal A and display current state
  modules->setSignalAspect(signalA, SignalAspectSz); //Preceed on sight authority
  printSignalsState();
  delay(2000);
}

void printSignalsState()
{
  modules->checkDeviceState(signalA);
  Serial.println("Signal A is " + nameOf(signalA->state));
}
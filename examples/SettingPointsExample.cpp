/*
 * Copyright (c) 2022 https://www.trainbrains.eu (Greg Barski).
 * This product is licensed under an GNU GPL, see the LICENSE file in the top-level directory.
 */

#include <Arduino.h>
#include "TrainbrainsModules.h"
#include "Point.h"

/*
 * In this example there is a layout with 3 points.
 *
 *  Infrastruture is:
 *  - Signle point driver at address 20
 *  - Double points driver at address 21
 *
 *  Points are connected to
 *  - point 1 driver is connected to the driver on address 20, channel 1
 *  - point 2 driver is connected to the driver on address 21, channel 1
 *  - point 3 driver is connected to the driver on address 21, channel 2
 */

TrainbrainsModulesRef modules = new TrainbrainsModules();
PointRef point1 = new Point(20, "Point 1");
PointRef point2 = new Point(21, 1, "Point 2");
PointRef point3 = new Point(21, 2, "Point 3");

void setup()
{
  modules->init();
  modules->switchPointPosition(point1, PointPosition::POSITION_PLUS);
  modules->switchPointPosition(point2, PointPosition::POSITION_PLUS);
  modules->switchPointPosition(point3, PointPosition::POSITION_PLUS);  
}

void loop()
{
  // Display switching point 1
  modules->switchPointPosition(point1, PointPosition::POSITION_PLUS); 
  modules->switchPointPosition(point3, PointPosition::POSITION_MINUS);
  delay(4000);

  // Display switching point 2
  modules->switchPointPosition(point2, PointPosition::POSITION_PLUS); 
  modules->switchPointPosition(point1, PointPosition::POSITION_MINUS);
  delay(4000);

  // Display switching point 3
  modules->switchPointPosition(point3, PointPosition::POSITION_PLUS); 
  modules->switchPointPosition(point2, PointPosition::POSITION_MINUS);
  delay(4000);
}
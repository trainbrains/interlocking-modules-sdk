/*
 * Copyright (c) 2022 https://www.trainbrains.eu (Greg Barski).
 * This product is licensed under an GNU GPL, see the LICENSE file in the top-level directory.
 */

#include <Arduino.h>

/*
  To allow using lambda expressions we need to add following definition in a main project file.
*/
#define ALLOW_USING_LAMBDAS USING_LAMBDAS_DISABLED

#include "TrainbrainsModules.h"
#include "PKPSignalAspects.h"

/*
 * In this example there is a layout with 1 track unoccupancy detector.
 * We are gonna display some signals aspects on given light signal when track get occupied or released.
 *
 *  Infrastruture is:
 *  - Signle light driver at address 10
 *  - Track unoccupancy detector at address 30
 *
 *  Light signal is connected to:
 *  - signal 1 is connected to the driver on address 10, channel 1
 *
 *  Isolated track is connected tothe detector on address 30, channel 1
 */

class AutomationExample
{

private:
  TrainbrainsModulesRef modules;

public:
  AutomationExample(TrainbrainsModulesRef aModules)
  {
    modules = aModules;
  }

  ~AutomationExample()
  {
    modules = NULL;
  }

  void restoreSignalAfterTrackHasBeenOccupied(LightSignalRef signal, TrackUnoccupancyDetectorRef trackOccupancyDetector, SignalAspect signalAspect)
  {
    trackOccupancyDetector->whenOccupied([this, signal, signalAspect](TrackUnoccupancyDetectorRef trackOccupancyDetector)
                                         { modules->setSignalAspect(signalA, signalAspect); });
  }

  void clearSignalAfterTrackHasBeenReleased(LightSignalRef signal, TrackUnoccupancyDetectorRef trackOccupancyDetector, SignalAspect signalAspect)
  {
    trackOccupancyDetector->whenReleased([this, signal, signalAspect](TrackUnoccupancyDetectorRef trackOccupancyDetector)
                                         { modules->setSignalAspect(signalA, signalAspect); });
  }
};


TrainbrainsModulesRef modules = new TrainbrainsModules();
LightSignalRef signalA = new LightSignal(10, "Signal A"); // Check the AdressingDeivesExample file;
TrackUnoccupancyDetectorRef track1 = new TrackUnoccupancyDetector(30, 1, "Track 1");
AutomationExample *myAutomation = new AutomationExample(modules);

void printObjectsState();

void setup()
{
  modules->init();
  modules->use(signalA);
  modules->use(track1);

  // Set some default signal aspect to observe changes
  modules->setSignalAspect(signalA, SignalAspectS2);

  // Note you can attach only one behaviour to each object's event
  myAutomation->restoreSignalAfterTrackHasBeenOccupied(signalA, track1, BasicSignalAspectStop);
  myAutomation->clearSignalAfterTrackHasBeenReleased(signalA, track1, BasicSignalAspectClear);
}

void loop()
{
  // Keep checking current state of devices
  modules->checkUsedObjectsState();
}

void printObjectsState()
{
  Serial.println("Track 1 is " + nameOf(track1->state));
  Serial.println("Signal A is " + nameOf(signalA->state));
}
/*
 * Copyright (c) 2022 https://www.trainbrains.eu (Greg Barski).
 * This product is licensed under an GNU GPL, see the LICENSE file in the top-level directory.
 */

#include <Arduino.h>
#include "TrainbrainsModules.h"
#include "PKPSignalAspects.h"

/*
 * In this example there is a layout with 1 track unoccupancy detector.
 * We are gonna display some signals aspects on given light signal when track get occupied of released.
 *
 *  Infrastruture is:
 *  - Signle light driver at address 10
 *  - Track unoccupancy detector at address 30
 *
 *  Light signal is connected to:
 *  - signal 1 is connected to the driver on address 10, channel 1
 * 
 *  Isolated track is connected tothe detector on address 30, channel 1
 */

TrainbrainsModulesRef modules = new TrainbrainsModules();
LightSignalRef signalA = new LightSignal(10, "Signal A"); //Check the AdressingDeivesExample file;
TrackUnoccupancyDetectorRef track1 = new TrackUnoccupancyDetector(30, 1, "Track 1");

void printObjectsState();

void setup()
{  
  modules->init();
  modules->use(signalA);
  modules->use(track1);

  // Set some default signal aspect to observe changes
  modules->setSignalAspect(signalA, SignalAspectS2);
  
  // When track occupancy change, handle new state
  track1->whenOccupied([](TrackUnoccupancyDetectorRef trackOccupancyDetector){ modules->setSignalAspect(signalA, SignalAspectS1);});
  track1->whenReleased([](TrackUnoccupancyDetectorRef trackOccupancyDetector){ modules->setSignalAspect(signalA, SignalAspectS5);});  
  track1->whenUnoccupancyChanged([](TrackUnoccupancyDetectorRef trackOccupancyDetector){ printObjectsState(); });  
}

void loop()
{
  // Keep checking current state of devices
  modules->checkUsedObjectsState();
}

void printObjectsState()
{
  Serial.println("Track 1 is " + nameOf(track1->state));
  Serial.println("Signal A is " + nameOf(signalA->state));
}

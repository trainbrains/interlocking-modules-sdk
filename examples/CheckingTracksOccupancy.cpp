/*
 * Copyright (c) 2023 https://www.trainbrains.eu (Greg Barski).
 * This product is licensed under an GNU GPL, see the LICENSE file in the top-level directory.
 */

#include <Arduino.h>
#include "TrainbrainsModules.h"

/*
 * In this example there is a layout with 4 track unoccupancy detectors.
 * We are gonna display check and display track occupancy information.
 *
 *  Infrastruture is:
 *  - Track unoccupancy detectors at address 30 
 *
 *  Isolated track are connected to the detector on address 30, channel 1, 2, 3 and 4.
 */

TrainbrainsModulesRef modules = new TrainbrainsModules();
TrackUnoccupancyDetectorRef track1 = new TrackUnoccupancyDetector(30, 1, "track 1");
TrackUnoccupancyDetectorRef track2 = new TrackUnoccupancyDetector(30, 2, "track 2");
TrackUnoccupancyDetectorRef track3 = new TrackUnoccupancyDetector(30, 3, "track 3");
TrackUnoccupancyDetectorRef track4 = new TrackUnoccupancyDetector(30, 4, "track 4");

void printObjectsState();

void setup()
{  
  Serial.begin(115200); 
  
  modules->init();
  modules->use(track1);
  modules->use(track2);
  modules->use(track3);
  modules->use(track4);


  track1->whenUnoccupancyChanged([](TrackUnoccupancyDetectorRef trackOccupancyDetector)
                                 { printObjectsState(); });
  track2->whenUnoccupancyChanged([](TrackUnoccupancyDetectorRef trackOccupancyDetector)
                                 { printObjectsState(); });
  track3->whenUnoccupancyChanged([](TrackUnoccupancyDetectorRef trackOccupancyDetector)
                                 { printObjectsState(); });
  track4->whenUnoccupancyChanged([](TrackUnoccupancyDetectorRef trackOccupancyDetector)
                                 { printObjectsState(); });
}

void loop()
{
  // Keep checking current state of devices
  modules->checkUsedObjectsState();
}

void printObjectsState()
{
  Serial.println("Track 1 is " + nameOf(track1->state));
  Serial.println("Track 2 is " + nameOf(track2->state));
  Serial.println("Track 3 is " + nameOf(track3->state));
  Serial.println("Track 4 is " + nameOf(track4->state));
}